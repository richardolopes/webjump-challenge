<?php if(!class_exists('Rain\Tpl')){exit;}?>  <!-- Main Content -->
  <main class="content">
  	<h1 class="title new-item">New Product</h1>

  	<form action="/editProduct.php" method="POST" enctype="multipart/form-data">
  		<div class="input-field">
  			<label for="id" class="label">Id Product</label>
  			<input type="text" readonly id="id" value="<?php echo htmlspecialchars( $product->getId(), ENT_COMPAT, 'UTF-8', FALSE ); ?>" name="id" class="input-text" />
  		</div>
  		<div class="input-field">
  			<label for="sku" class="label">Product SKU</label>
  			<input type="text" id="sku" value="<?php echo htmlspecialchars( $product->getSku(), ENT_COMPAT, 'UTF-8', FALSE ); ?>" maxlength="9" name="sku" class="input-text" />
  		</div>
  		<div class="input-field">
  			<label for="name" class="label">Product Name</label>
  			<input type="text" id="name" value="<?php echo htmlspecialchars( $product->getName(), ENT_COMPAT, 'UTF-8', FALSE ); ?>" maxlength="130" name="name" class="input-text" />
  		</div>
  		<div class="input-field">
  			<label for="price" class="label">Price</label>
  			<input type="text" id="price" value="<?php echo htmlspecialchars( $product->getPrice(), ENT_COMPAT, 'UTF-8', FALSE ); ?>" name="price" class="input-text" />
  		</div>
  		<div class="input-field">
  			<label for="quantity" class="label">Quantity</label>
  			<input type="text" id="quantity" value="<?php echo htmlspecialchars( $product->getAmount(), ENT_COMPAT, 'UTF-8', FALSE ); ?>" maxlength="3" name="quantity" class="input-text" />
  		</div>
  		<div class="input-field">
  			<label for="category" class="label">Categories</label>
  			<select multiple id="category" name="category" class="input-text">
  				<?php $counter1=-1;  if( isset($categories) && ( is_array($categories) || $categories instanceof Traversable ) && sizeof($categories) ) foreach( $categories as $key1 => $value1 ){ $counter1++; ?>
				  <option value="<?php echo htmlspecialchars( $value1["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>"><?php echo htmlspecialchars( $value1["name"], ENT_COMPAT, 'UTF-8', FALSE ); ?></option>
  				<?php } ?>
  			</select>
  		</div>
  		<div class="input-field">
  			<label for="description" class="label">Description</label>
  			<textarea id="description" maxlength="150" name="description" class="input-text"><?php echo htmlspecialchars( $product->getDesc(), ENT_COMPAT, 'UTF-8', FALSE ); ?></textarea>
  		</div>
  		<div class="input-field">
  			<label for="image" class="label">Image</label>
  			<input type="file" id="image" name="image" class="input-field"></input>
  		</div>
  		<div class="actions-form">
  			<a href="products.php" class="action back">Back</a>
  			<input class="btn-submit btn-action" type="submit" value="Save Product" />
  		</div>

  	</form>
  </main>
  <!-- Main Content -->