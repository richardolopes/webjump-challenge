# Configurações:
- Banco de dados MySQL com o nome webjump;
- Importar o arquivo "webjump.sql";


# Tecnologias utilizadas:
- RainTpl - Utilizado para construir o template.
- Sweetalert - Utilizado para mostrar mensagens mais amigáveis.


# Páginas com interação do usuário:
- /index.php - Página inicial - dashboard;
- /addProduct.php - Página para adicionar produtos;
- /addCategory.php - Página de adicionar categorias;
- /products - Página para mostrar todos os produtos inseridos no banco de dados;
- /categories - Página para mostrar todas as categorias inseridas no banco de dados;

# Páginas para controle:
- /processProducts.php - Página para importar o arquivo;
- /editCategory.php?id= - Página para alterar a categoria;
- /editProduct.php?id= - Página para alterar o produto;
- /deleteCategory.php?id= - Página para deletar as relações da categoria com os produtos e, em seguida, deleta a categoria;
- /deleteProduct.php?id= - Página para deletar as relações do produto com as categorias e, em seguida, deleta o produto;
- /functions.php - Funções para tratar as informações utilizadas no template.
