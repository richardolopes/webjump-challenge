<?php

namespace Webjump\Model;

class Category
{
    public $name;
    public $code;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->code;
    }

    public function setId($code)
    {
        $this->code = $code;
    }

}
