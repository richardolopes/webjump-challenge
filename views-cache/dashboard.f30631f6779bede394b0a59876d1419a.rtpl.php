<?php if(!class_exists('Rain\Tpl')){exit;}?>  <!-- Main Content -->
  <main class="content">
  	<div class="header-list-page">
  		<h1 class="title">Dashboard</h1>
  	</div>
  	<div class="infor">
  		You have <?php echo htmlspecialchars( count($products), ENT_COMPAT, 'UTF-8', FALSE ); ?> products added on this store: <a href="/addProduct.php" class="btn-action">Add new Product</a>
  	</div>
  	<ul class="product-list">
  		<?php $counter1=-1;  if( isset($products) && ( is_array($products) || $products instanceof Traversable ) && sizeof($products) ) foreach( $products as $key1 => $value1 ){ $counter1++; ?>
  		<li>
  			<div class="product-image">
  				<img src="<?php echo htmlspecialchars( $value1["image"], ENT_COMPAT, 'UTF-8', FALSE ); ?>" layout="responsive" width="164" height="145" alt="<?php echo htmlspecialchars( $value1["name"], ENT_COMPAT, 'UTF-8', FALSE ); ?>" />
  			</div>
  			<div class="product-info">
  				<div class="product-name"><span><?php echo htmlspecialchars( formatName($value1["name"]), ENT_COMPAT, 'UTF-8', FALSE ); ?></span></div>
  				<div class="product-price"><span>R$ <?php echo htmlspecialchars( formatPrice($value1["price"]), ENT_COMPAT, 'UTF-8', FALSE ); ?></span></div>
  			</div>
  		</li>
  		<?php } ?>
  		<li>
  			<div class="product-image">
  				<a href="tenis-basket-light.html" title="Tênis Basket Light">
  					<img src="/assets/images/product/tenis-basket-light.png" layout="responsive" width="164"
  						height="145" alt="Tênis Basket Light" />
  				</a>
  			</div>
  			<div class="product-info">
  				<div class="product-name"><span>Tênis Basket Light</span></div>
  				<div class="product-price"><span class="special-price">1 available</span> <span>R$459,99</span></div>
  			</div>
  		</li>
  		<li>
  			<div class="product-image">
  				<a href="tenis-basket-light.html" title="Tênis Basket Light">
  					<img src="/assets/images/product/tenis-2d-shoes.png" layout="responsive" width="164" height="145"
  						alt="Tênis 2D Shoes" />
  				</a>
  			</div>
  			<div class="product-info">
  				<div class="product-name"><span>Tênis 2D Shoes</span></div>
  				<div class="product-price"><span class="special-price">2 Available</span> <span>R$459,99</span></div>
  			</div>
  		</li>
  		<li>
  			<div class="product-image">
  				<img src="/assets/images/product/tenis-sneakers-43n.png" layout="responsive" width="164" height="145"
  					alt="Tênis Sneakers 43N" />
  			</div>
  			<div class="product-info">
  				<div class="product-name"><span>Tênis Sneakers 43N</span></div>
  				<div class="product-price"><span class="special-price">Out of stock</span> <span>R$459,99</span></div>
  			</div>
  		</li>
  	</ul>
  </main>
  <!-- Main Content -->