<?php

session_start();

$_SESSION["page"] = "products.php";

require_once "vendor/autoload.php";

use \Webjump\Controller\CategoryProductPDO;
use \Webjump\Controller\Page;
use \Webjump\Controller\ProductPDO;
use \Webjump\Controller\User;
use \Webjump\Model\Product;
use \Webjump\View\CategoryView;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    foreach ($_POST as $key => $value) {
        (empty($_POST[$key]) || !isset($_POST[$key])) ? User::setMsg("NOTDEFINED_PRODUCT") : "";
    }

    $file = $_FILES["image"];
    $dirUploads = "assets" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "product";

    is_dir($dirUploads) ? "" : mkdir($dirUploads);

    if (!$file["error"]) {
        // diretório completo para ser gravado no banco + tempo (para tratamento de imagens com os nomes iguais) + nome do arquivo com a extensao
        $file["name"] = $dirUploads . DIRECTORY_SEPARATOR . time() . $file["name"];
    }

    if (!move_uploaded_file($file["tmp_name"], $file["name"])) {
        $file["name"] = "";
    }

    $product = new Product();
    $product->setId($_POST["id"]);
    $product->setSku($_POST["sku"]);
    $product->setName($_POST["name"]);
    $product->setPrice($_POST["price"]);
    $product->setAmount($_POST["quantity"]);
    $product->setDesc($_POST["description"]);
    $product->setImage($file["name"]);

    $productPDO = new ProductPDO();
    $productPDO->validateProduct($product);

    $product = $productPDO->update($product);

    $rcp = new CategoryProductPDO();
	$rcp->deleteRelProduct($product->getId());
	
	if (!empty($_POST["category"]) && isset($_POST["category"])) {
		$rcp->insert($_POST["category"], $product->getId());
	};

    if (!empty($product) && isset($product)) {
        User::setMsg("EDITPRODUCT_SUCCESS");
    } else {
        User::setMsg("EDITPRODUCT_ERROR");
    }
} else {
    $categories = CategoryView::listCategories();
    $productPDO = new ProductPDO();
    $product = $productPDO->load($_GET["id"]);

    $page = new Page(["title" => "Edit Product"]);
    $page->setTpl("editProduct", array(
        "product" => $product,
        "categories" => $categories,
    ));
}
