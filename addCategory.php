<?php

session_start();

$_SESSION["page"] = "addCategory.php";

require_once "vendor/autoload.php";

use \Webjump\Controller\CategoryPDO;
use \Webjump\Controller\Page;
use \Webjump\Controller\User;
use \Webjump\Model\Category;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST["category-name"]) && isset($_POST["category-name"])) {
        $category = new Category();
        $category->setName($_POST["category-name"]);

        $categoryPDO = new CategoryPDO();
        $categoryPDO->validateCategory($category);

        $category = $categoryPDO->insert($category);

        if (!empty($category) && isset($category)) {
            User::setMsg("ADDCATEGORY_SUCCESS");
        } else {
            User::setMsg("ADDCATEGORY_ERROR");
        }
    } else {
        User::setMsg("NOTDEFINED_CATEGORY");
    }
} else {
    $page = new Page(["title" => "Add Category"]);
    $page->setTpl("addCategory");
}
