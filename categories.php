<?php

session_start();

$_SESSION["page"] = "categories.php";

require_once "vendor/autoload.php";
require_once "functions.php";

use \Webjump\Controller\Page;
use \Webjump\View\CategoryView;

$categories = CategoryView::listCategories();

$page = new Page(["title" => "Categories"]);
$page->setTpl("categories", array(
    "categories" => $categories,
));
