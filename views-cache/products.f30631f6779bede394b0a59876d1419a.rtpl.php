<?php if(!class_exists('Rain\Tpl')){exit;}?>  <!-- Main Content -->
  <main class="content">
  	<div class="header-list-page">
  		<h1 class="title">Products</h1>
  		<a href="/addProduct.php" class="btn-action">Add new Product</a>
  	</div>
  	<table class="data-grid">
  		<tr class="data-row">
  			<th class="data-grid-th">
  				<span class="data-grid-cell-content">Image</span>
  			</th>
  			<th class="data-grid-th">
  				<span class="data-grid-cell-content">Name</span>
  			</th>
  			<th class="data-grid-th">
  				<span class="data-grid-cell-content">SKU</span>
  			</th>
  			<th class="data-grid-th">
  				<span class="data-grid-cell-content">Price</span>
  			</th>
  			<th class="data-grid-th">
  				<span class="data-grid-cell-content">Quantity</span>
  			</th>
  			<th class="data-grid-th">
  				<span class="data-grid-cell-content">Categories</span>
  			</th>

  			<th class="data-grid-th">
  				<span class="data-grid-cell-content">Actions</span>
  			</th>
  		</tr>
  		<?php $counter1=-1;  if( isset($products) && ( is_array($products) || $products instanceof Traversable ) && sizeof($products) ) foreach( $products as $key1 => $value1 ){ $counter1++; ?>
  		<tr class="data-row">
  			<td class="data-grid-td">
  				<span class="data-grid-cell-content"><img src="<?php echo htmlspecialchars( $value1["image"], ENT_COMPAT, 'UTF-8', FALSE ); ?>" width="100px" height="100px"
  						alt=""></span>
  			</td>

  			<td class="data-grid-td">
  				<span class="data-grid-cell-content"><?php echo htmlspecialchars( $value1["name"], ENT_COMPAT, 'UTF-8', FALSE ); ?></span>
  			</td>

  			<td class="data-grid-td">
  				<span class="data-grid-cell-content"><?php echo htmlspecialchars( $value1["sku"], ENT_COMPAT, 'UTF-8', FALSE ); ?></span>
  			</td>

  			<td class="data-grid-td">
  				<span class="data-grid-cell-content">R$ <?php echo formatPrice($value1["price"]); ?></span>
  			</td>

  			<td class="data-grid-td">
  				<span class="data-grid-cell-content"><?php echo htmlspecialchars( $value1["amount"], ENT_COMPAT, 'UTF-8', FALSE ); ?></span>
  			</td>

  			<td class="data-grid-td">
  				<span class="data-grid-cell-content">
  					<?php $counter2=-1;  if( isset($value1["categories"]) && ( is_array($value1["categories"]) || $value1["categories"] instanceof Traversable ) && sizeof($value1["categories"]) ) foreach( $value1["categories"] as $key2 => $value2 ){ $counter2++; ?>
  					<?php echo htmlspecialchars( $value2["name"], ENT_COMPAT, 'UTF-8', FALSE ); ?><br>
  					<?php } ?>
  				</span>
  			</td>

  			<td class="data-grid-td">
  				<div class="actions">
  					<div class="action edit"><a href="/editProduct.php?id=<?php echo htmlspecialchars( $value1["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>"><span>Edit</span></a></div>
  					<div class="action delete"><a class="onclick" onclick='deletar("/deleteProduct.php?id=<?php echo htmlspecialchars( $value1["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>")'><span>Delete</span></a>
  					</div>
  				</div>
  			</td>
  		</tr>
  		<?php } ?>
  	</table>
  </main>
  <!-- Main Content -->