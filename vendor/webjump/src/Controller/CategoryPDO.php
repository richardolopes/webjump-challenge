<?php

namespace Webjump\Controller;

use \Webjump\Controller\CategoryProductPDO;
use \Webjump\Controller\Connection;
use \Webjump\Controller\User;
use \Webjump\Model\Category;

class CategoryPDO
{
    public function load($id = int)
    {
        if (!empty($id) && isset($id)) {
            $mysql = new Connection();
            $return = $mysql->select("SELECT `id`, `name` FROM `category` WHERE `id`=:ID", array(
                ":ID" => $id,
            ));

            if (count($return[0]) > 0) {
                $category = new Category();
                $category->setId($return[0]["id"]);
                $category->setName($return[0]["name"]);

                return $category;
            }
        }
    }

    public function insert($category)
    {
        if (!empty($category) && isset($category)) {
            $mysql = new Connection();
            $return = $mysql->select("SELECT `id`, `name` FROM `category` WHERE `name` = :NAMEC", array(
                ":NAMEC" => $category->getName(),
            ));

            if (@count($return[0]) <= 0) {
                try {
                    $mysql->query("INSERT INTO `category`(`name`) VALUES (:NAMEC)", array(
                        ":NAMEC" => $category->getName(),
                    ));
                } catch (PDOException $e) {
                    User::setMsg("NOTINSERTED_CATEGORY");
                }

                $return = $mysql->select("SELECT `id`, `name` FROM `category` WHERE `name` = :NAMEC", array(
                    ":NAMEC" => $category->getName(),
                ));

                if (count($return[0]) > 0) {
                    return $this->load($return[0]["id"]);
                } else {
                    User::setMsg("NOTINSERTED_CATEGORY");
                }
            } else {
                return $this->load($return[0]["id"]);
            }
        } else {
            User::setMsg("NOTDEFINED_CATEGORY");
        }
    }

    public function delete($category)
    {
        if (!empty($category) && isset($category)) {
            $rcp = new CategoryProductPDO();
            $rcp->deleteRelCategory($category->getId());

            $mysql = new Connection();
            try {
                $mysql->query("DELETE FROM `category` WHERE `id` = :ID", array(
                    ":ID" => $category->getId(),
                ));
            } catch (PDOException $e) {
                User::setMsg("NOTDELETED_CATEGORY");
            }
        } else {
            User::setMsg("NOTDEFINED_CATEGORY");
        }
    }

    public function update($category)
    {
        if (!empty($category) && isset($category)) {
            $mysql = new Connection();
            try {
                $mysql->query("UPDATE `category` SET `name`=:NAMEC WHERE id = :ID", array(
                    ":ID" => $category->getId(),
                    ":NAMEC" => $category->getName(),
                ));
            } catch (PDOException $e) {
                User::setMsg("NOTUPDATED_CATEGORY");
            }

            return $this->load($category->getId());

        } else {
            User::setMsg("NOTDEFINED_CATEGORY");
        }
    }

    public function validateCategory($category)
    {
        if (!empty($category) && isset($category)) {
            strlen($category->getName()) > 130 ? User::setMsg("CATEGORYPDO_SKULENGTH") : "";
        } else {
            User::setMsg("NOTDEFINED_CATEGORY");
        }
    }
}
