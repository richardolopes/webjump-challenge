<?php

session_start();

$_SESSION["page"] = "categories.php";

require_once "vendor/autoload.php";

use \Webjump\Controller\CategoryPDO;
use \Webjump\Controller\Page;
use \Webjump\Controller\User;
use \Webjump\Model\Category;
use \Webjump\View\CategoryView;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    foreach ($_POST as $key => $value) {
        (empty($_POST[$key]) || !isset($_POST[$key])) ? User::setMsg("NOTDEFINED_CATEGORY") : "";
    }

    $category = new Category();
    $category->setId($_POST["id"]);
    $category->setName($_POST["category-name"]);

    $categoryPDO = new CategoryPDO();
    $categoryPDO->validateCategory($category);

    $category = $categoryPDO->update($category);

    if (!empty($category) && isset($category)) {
        User::setMsg("EDITCATEGORY_SUCCESS");
    } else {
        User::setMsg("EDITCATEGORY_ERROR");
    }
} else {
    $categoryPDO = new CategoryPDO();
    $category = $categoryPDO->load($_GET["id"]);

    $page = new Page(["title" => "Edit Category"]);
    $page->setTpl("editCategory", array(
        "category" => $category,
    ));
}
