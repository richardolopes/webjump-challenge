<?php

namespace Webjump\Model;

class Product
{
    public $id;
    public $sku;
    public $name;
    public $desc;
    public $price;
    public $amount;
    public $image;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSku()
    {
        return $this->code;
    }

    public function setSku($code)
    {
        $this->code = intval(str_replace("-", "", $code));
    }

    public function getDesc()
    {
        return $this->desc;
    }

    public function setDesc($desc)
    {
        $this->desc = $desc;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setImage($image)
    {
        $dir = "assets" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "product" . DIRECTORY_SEPARATOR . "no-image.jpg";
        $this->image = empty($image) ? $dir : $image;
    }

    public function getImage()
    {
        return $this->image;
    }
}
