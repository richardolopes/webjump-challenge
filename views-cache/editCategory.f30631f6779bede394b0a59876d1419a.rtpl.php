<?php if(!class_exists('Rain\Tpl')){exit;}?><!-- Main Content -->
<main class="content">
	<h1 class="title new-item">New Category</h1>

	<form action="/editCategory.php" method="POST">
		<div class="input-field">
			<label for="id" class="label">Id Category</label>
			<input type="text" readonly value="<?php echo htmlspecialchars( $category->getId(), ENT_COMPAT, 'UTF-8', FALSE ); ?>" id="id" name="id" class="input-text" />
		</div>
		<div class="input-field">
			<label for="category-name" class="label">Category Name</label>
			<input type="text" value="<?php echo htmlspecialchars( $category->getName(), ENT_COMPAT, 'UTF-8', FALSE ); ?>" id="category-name" name="category-name" class="input-text" />
		</div>
		<div class="actions-form">
			<a href="/categories.php" class="action back">Back</a>
			<input class="btn-submit btn-action" type="submit" value="Save" />
		</div>
	</form>
</main>
<!-- Main Content -->