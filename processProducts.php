<?php

require_once "vendor/autoload.php";

use \Webjump\Controller\CategoryPDO;
use \Webjump\Controller\CategoryProductPDO;
use \Webjump\Controller\ProductPDO;
use \Webjump\Model\Category;
use \Webjump\Model\Product;

$file = "./assets/import.csv";

if (file_exists($file)) {
    set_time_limit(3600);
    $file = fopen($file, "r");

    $header = explode(";", fgetcsv($file)[0]);

    while ($line = fgetcsv($file, 0, ";")) {
        $product = new Product();
        $product->setName($line[0]);
        $product->setSku($line[1]);
        $product->setDesc($line[2]);
        $product->setAmount($line[3]);
        $product->setPrice($line[4]);
        $product->setImage("");

        $category = new Category();
        $categoryPDO = new CategoryPDO();

        if (empty($line[5])) {
            $category->setName("(no genres listed)");
        } else {
            $idCategories = array();
            $categories = explode("|", $line[5]);

            for ($i = 0; $i < count($categories); $i++) {
                $category->setName($categories[$i]);

                $category = $categoryPDO->insert($category);

                array_push($idCategories, $category->getId());
            }
        }

        $productPDO = new ProductPDO();
        $product = $productPDO->insert($product);

        // Relação Categoria Produto
        $rcp = new CategoryProductPDO();

        for ($i = 0; $i < count($idCategories); $i++) {
            $rcp->insert($idCategories[$i], $product->getId());
        }

    }
} else {
    // User::setMsg("NOTFOUND_IMPORT");
    die("O arquivo de importação não foi encontrado");
}
