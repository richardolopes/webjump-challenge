<?php

namespace Webjump\View;

use \Webjump\Controller\Connection;

class CategoryView
{
    public static function listCategories(): array
    {
        $mysql = new Connection();
        return $mysql->select("SELECT `id`, `name` FROM `category`");
    }
}
