<?php

session_start();

$_SESSION["page"] = "index.php";

require_once "vendor/autoload.php";
require_once "functions.php";

use \Webjump\Controller\Page;
use \Webjump\View\ProductView;

$productView = new ProductView();
$products = $productView->listProducts();

$page = new Page(["title" => "Dashboard"]);
$page->setTpl("dashboard", array(
    "products" => $products,
));
