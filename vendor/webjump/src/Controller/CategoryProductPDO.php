<?php

namespace Webjump\Controller;

use \Webjump\Controller\Connection;
use \Webjump\Controller\User;

class CategoryProductPDO
{
    public function insert($idcategory, $idproduct)
    {
        if (!empty($idcategory) && isset($idcategory) && !empty($idproduct) && isset($idproduct)) {
            $mysql = new Connection();
            $return = $mysql->select("SELECT `id` FROM `category_product` WHERE `idproduct` = :IDPROD AND `idcategory` = :IDCAT", array(
                ":IDPROD" => $idproduct,
                ":IDCAT" => $idcategory,
            ));

            if (count($return) <= 0) {
                try {
                    $mysql->query("INSERT INTO `category_product`(`idproduct`, `idcategory`) VALUES (:IDPROD, :IDCAT)", array(
                        ":IDPROD" => $idproduct,
                        ":IDCAT" => $idcategory,
                    ));
                } catch (PDOException $e) {
                    User::setMsg("NOTINSERTED_REL");
                }
            }
        } else {
            User::setMsg("NOTDEFINED_REL");
        }
    }

    public function deleteRelProduct($idproduct)
    {
        if (!empty($idproduct) && isset($idproduct)) {
            $mysql = new Connection();
            try {
                $mysql->query("DELETE FROM `category_product` WHERE `idproduct` = :ID", array(
                    ":ID" => $idproduct,
                ));
            } catch (PDOException $e) {
                User::setMsg("NOTDELETED_REL");
            }
        } else {
            User::setMsg("NOTDEFINED_REL");
        }
    }

    public function deleteRelCategory($idcategory)
    {
        if (!empty($idcategory) && isset($idcategory)) {
            $mysql = new Connection();
            try {
                $mysql->query("DELETE FROM `category_product` WHERE `idcategory` = :ID", array(
                    ":ID" => $idcategory,
                ));
            } catch (PDOException $e) {
                User::setMsg("NOTDELETED_REL");
            }
        } else {
            User::setMsg("NOTDEFINED_REL");
        }
    }

    public function delete($idcategory, $idproduct)
    {
        if (!empty($idcategory) && isset($idcategory) && !empty($idproduct) && isset($idproduct)) {
            $mysql = new Connection();
            try {
                $mysql->query("DELETE FROM `category_product` WHERE `idcategory` = :IDCAT AND `idproduct` = :IDPROD", array(
                    ":IDCAT" => $idcategory,
                    ":IDPROD" => $idproduct,
                ));
            } catch (PDOException $e) {
                User::setMsg("NOTDELETED_REL");
            }
        } else {
            User::setMsg("NOTDEFINED_REL");
        }
    }
}
