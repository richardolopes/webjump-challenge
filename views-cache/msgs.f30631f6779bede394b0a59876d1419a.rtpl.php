<?php if(!class_exists('Rain\Tpl')){exit;}?><?php if( $msg != '' ){ ?>

<!-- PRODUCT -->
<?php if( $msg == 'NOTDEFINED_PRODUCT' ){ ?>
<script>
	swal("Produto não definido.", "", "error");
	</script>
<?php } ?>

<?php if( $msg == 'NOTFOUND_PRODUCT' ){ ?>
<script>
	swal("Produto não encontrado.", "", "info");
	</script>
<?php } ?>

<?php if( $msg == 'NOTDELETED_PRODUCT' ){ ?>
<script>
	swal("Produto não deletado.", "", "warning");
	</script>
<?php } ?>

<?php if( $msg == 'NOTINSERTED_PRODUCT' ){ ?>
<script>
	swal("Produto não inserido.", "", "info");
	</script>
<?php } ?>

<?php if( $msg == 'NOTUPDATED_PRODUCT' ){ ?>
<script>
	swal("Produto não alterado.", "", "warning");
	</script>
<?php } ?>


<!-- PRODUCT PDO -->
<?php if( $msg == 'PRODUCTPDO_SKULENGTH' ){ ?>
<script>
	swal("O código(sku) do produto deve ser menor que 10 dígitos.", "", "error");
</script>
<?php } ?>
<?php if( $msg == 'PRODUCTPDO_NAMELENGTH' ){ ?>
<script>
	swal("O nome do produto deve ser menor que 130 caracteres.", "", "error");
</script>
<?php } ?>
<?php if( $msg == 'PRODUCTPDO_AMOUNTLENGTH' ){ ?>
<script>
	swal("A quantidade de produtos deve ser menor que 999 itens.", "", "error");
</script>
<?php } ?>
<?php if( $msg == 'PRODUCTPDO_DESCRIPTIONLENGTH' ){ ?>
<script>
	swal("A descrição do produto deve ser menor que 150 caracteres.", "", "error");
</script>
<?php } ?>


<!-- CATEGORY -->
<?php if( $msg == 'NOTDEFINED_CATEGORY' ){ ?>
<script>
	swal("Categoria não definida.", "", "error");
</script>
<?php } ?>
<?php if( $msg == 'NOTFOUND_CATEGORY' ){ ?>
<script>
	swal("Categoria não econtrada.", "", "info");
</script>
<?php } ?>
<?php if( $msg == 'NOTINSERTED_CATEGORY' ){ ?>
<script>
	swal("Categoria não inserida.", "", "info");
</script>
<?php } ?>
<?php if( $msg == 'NOTDELETED_CATEGORY' ){ ?>
<script>
	swal("Categoria não deletada.", "", "warning");
</script>
<?php } ?>
<?php if( $msg == 'NOTUPDATED_CATEGORY' ){ ?>
<script>
	swal("Categoria não atualizado.", "", "warning");
</script>
<?php } ?>


<!-- CATEGORY PDO -->
<?php if( $msg == 'CATEGORYPDO_SKULENGTH' ){ ?>
<script>
	swal("O nome da categoria deve ter menos de 130 caracteres.", "", "error");
</script>
<?php } ?>


<!-- CATEGORY PRODUCT -->
<?php if( $msg == 'NOTDEFINED_REL' ){ ?>
<script>
	swal("Relação não definida.", "", "error");
</script>
<?php } ?>
<?php if( $msg == 'NOTDELETED_REL' ){ ?>
<script>
	swal("Relação não deletada.", "", "warning");
</script>
<?php } ?>
<?php if( $msg == 'NOTINSERTED_REL' ){ ?>
<script>
	swal("Relação não incluída.", "", "info");
</script>
<?php } ?>



<!-- ADD CATEGORY -->
<?php if( $msg == 'ADDCATEGORY_SUCCESS' ){ ?>
<script>
	swal("Categoria cadastrada com sucesso!", "", "success");
</script>
<?php } ?>

<?php if( $msg == 'ADDCATEGORY_ERROR' ){ ?>
<script>
	swal("Categoria não cadastrada.", "", "error");
</script>
<?php } ?>

<?php if( $msg == 'ADDCATEGORY_CATEGORYNAMELENGTH' ){ ?>
<script>
	swal("O nome da categoria deve ser menor que 70 caracteres.", "", "error");
	</script>
<?php } ?>


<!-- ADD PRODUCT -->
<?php if( $msg == 'ADDPRODUCT_SUCCESS' ){ ?>
<script>
	swal("Produto cadastrado com sucesso!", "", "success");
</script>
<?php } ?>
<?php if( $msg == 'ADDPRODUCT_ERROR' ){ ?>
<script>
	swal("Produto não cadastrado.", "", "error");
</script>
<?php } ?>
<?php if( $msg == 'ADDPRODUCT_ERRORFILE' ){ ?>
<script>
	swal("Imagem do produto não enviada.", "", "error");
</script>
<?php } ?>


<!-- EDIT PRODUCT -->
<?php if( $msg == 'EDITPRODUCT_SUCCESS' ){ ?>
<script>
	swal("Produto alterado com sucesso!", "", "success");
</script>
<?php } ?>
<?php if( $msg == 'EDITPRODUCT_ERROR' ){ ?>
<script>
	swal("Produto não alterado.", "", "error");
</script>
<?php } ?>


<!-- EDIT CATEGORY -->
<?php if( $msg == 'EDITCATEGORY_SUCCESS' ){ ?>
<script>
	swal("Categoria alterada com sucesso!", "", "success");
</script>
<?php } ?>
<?php if( $msg == 'EDITCATEGORY_ERROR' ){ ?>
<script>
	swal("Categoria não alterada.", "", "error");
</script>
<?php } ?>


<!-- PROCESS PRODUCT -->
<?php if( $msg == 'NOTFOUND_IMPORT' ){ ?>
<script>
	swal("Arquivo não encontrado.", "", "info");
</script>
<?php } ?>


<?php } ?>