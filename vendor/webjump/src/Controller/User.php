<?php

namespace Webjump\Controller;

class User
{
    const SESSION = "User";
    const MSG = "UserMsg";

    public static function setMsg($msg)
    {
        $_SESSION[User::MSG] = $msg;

        if (!empty($_SESSION["page"]) && isset($_SESSION["page"])) {
            header("Location: /" . $_SESSION["page"]);
        } else {
            header("Location: /");
        }

        exit;
    }

    public static function getMsg()
    {
        $msg = (isset($_SESSION[User::MSG]) && $_SESSION[User::MSG]) ? $_SESSION[User::MSG] : "";
        User::clearMsg();
        return $msg;
    }

    public static function clearMsg()
    {
        $_SESSION[User::MSG] = null;
    }
}
