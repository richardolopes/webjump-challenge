<?php

session_start();

$_SESSION["page"] = "categories.php";

require_once "vendor/autoload.php";

use \Webjump\Controller\CategoryPDO;
use \Webjump\Controller\CategoryProductPDO;
use \Webjump\Model\Category;

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (!empty($_GET["id"]) && isset($_GET["id"])) {
        $category = new Category();
        $category->setId($_GET["id"]);

        $rcp = new CategoryProductPDO();
        $rcp->deleteRelProduct($category->getId());

        $productPDO = new CategoryPDO();
        $productPDO->delete($category);

        $category = $productPDO->load($category->getId());

        if (empty($category) && !isset($category)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
