<?php

namespace Webjump\View;

use \Webjump\Controller\Connection;

class ProductView
{
    public static function listProducts(): array
    {
        $mysql = new Connection();
        $return = $mysql->select("SELECT prod.id, prod.sku, prod.name, prod.description, prod.amount, prod.price, prod.image FROM `product` AS prod");

        for ($i = 0; $i < count($return); $i++) {
            $categories = $mysql->select("SELECT cat.name FROM category_product AS catprod INNER JOIN category AS cat ON cat.id = catprod.idcategory WHERE `idproduct` = :ID", array(
                ":ID" => $return[$i]["id"],
            ));

            $return[$i]["categories"] = $categories;
        }

        return $return;
    }
}
