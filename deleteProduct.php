<?php

session_start();

$_SESSION["page"] = "products.php";

require_once "vendor/autoload.php";

use \Webjump\Controller\CategoryProductPDO;
use \Webjump\Controller\ProductPDO;
use \Webjump\Model\Product;
use \Webjump\View\CategoryView;

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (!empty($_GET["id"]) && isset($_GET["id"])) {
        $product = new Product();
        $product->setId($_GET["id"]);

        $rcp = new CategoryProductPDO();
        $rcp->deleteRelProduct($product->getId());

        $productPDO = new ProductPDO();
        $productPDO->delete($product);

        $product = $productPDO->load($product->getId());

        if (empty($product) && !isset($product)) {
            // User::setMsg("EDITPRODUCT_SUCCESS");
            return true;
        } else {
            // User::setMsg("EDITPRODUCT_ERROR");
            return false;
        }
    } else {
        // User::setMsg("NOTDEFINED_PRODUCT");
        return false;
    }
}
