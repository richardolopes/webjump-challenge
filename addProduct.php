<?php

session_start();

$_SESSION["page"] = "addProduct.php";

require_once "vendor/autoload.php";

use \Webjump\Controller\CategoryProductPDO;
use \Webjump\Controller\Page;
use \Webjump\Controller\ProductPDO;
use \Webjump\Controller\User;
use \Webjump\Model\Product;
use \Webjump\View\CategoryView;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    foreach ($_POST as $key => $value) {
        (empty($_POST[$key]) || !isset($_POST[$key])) ? User::setMsg("NOTDEFINED_PRODUCT") : "";
    }

    $file = $_FILES["image"];
    $dirUploads = "assets" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "product";

    is_dir($dirUploads) ? "" : mkdir($dirUploads);

    if (!$file["error"]) {
        // diretório completo para ser gravado no banco + tempo (para tratamento de imagens com os nomes iguais) + nome do arquivo com a extensao
        $file["name"] = $dirUploads . DIRECTORY_SEPARATOR . time() . $file["name"];
    }

    if (!move_uploaded_file($file["tmp_name"], $file["name"])) {
        $file["name"] = "";
    }

    $product = new Product();
    $product->setSku($_POST["sku"]);
    $product->setName($_POST["name"]);
    $product->setPrice($_POST["price"]);
    $product->setAmount($_POST["quantity"]);
    $product->setDesc($_POST["description"]);
    $product->setImage($file["name"]);

    $productPDO = new ProductPDO();
    $productPDO->validateProduct($product);

    $product = $productPDO->insert($product);

    if (!empty($_POST["category"]) && isset($_POST["category"])) {
        $rcp = new CategoryProductPDO();
        $rcp->insert($_POST["category"], $product->getId());
    }

    if (!empty($product) && isset($product)) {
        User::setMsg("ADDPRODUCT_SUCCESS");
    } else {
        User::setMsg("ADDPRODUCT_ERROR");
    }
} else {
    $categories = CategoryView::listCategories();

    $page = new Page(["title" => "Add Product"]);
    $page->setTpl("addProduct", array(
        "categories" => $categories,
    ));
}
