<?php

function formatPrice($vlprice)
{
    ($vlprice < 0) ? $vlprice = 0 : "";
    return number_format($vlprice, 2, ",", ".");
}

function formatName($name)
{
    $name = strlen($name) > 18 ? substr($name, 0, 15) . "..." : $name;
    return $name;
}
