<?php

namespace Webjump\Controller;

use \Webjump\Controller\CategoryProductPDO;
use \Webjump\Controller\Connection;
use \Webjump\Controller\User;
use \Webjump\Model\Product;

class ProductPDO
{
    public function load($id = int)
    {
        if (!empty($id) && isset($id)) {
            $mysql = new Connection();
            $return = $mysql->select("SELECT `id`, `sku`, `name`, `description`, `amount`, `price`, `image` FROM `product` WHERE `id` = :ID", array(
                ":ID" => $id,
            ));

            if (@count($return[0]) > 0) {
                $product = new Product();
                $product->setId($return[0]["id"]);
                $product->setSku($return[0]["sku"]);
                $product->setName($return[0]["name"]);
                $product->setDesc($return[0]["description"]);
                $product->setAmount($return[0]["amount"]);
                $product->setPrice($return[0]["price"]);
                $product->setImage($return[0]["image"]);

                return $product;
            }
        }
    }

    public function insert($product)
    {
        if (!empty($product) && isset($product)) {
            $mysql = new Connection();
            try {
                $mysql->query("INSERT INTO `product`(`sku`, `name`, `description`, `amount`, `price`, `image`) VALUES (:SKU, :NAMEP, :DESCRIPTIONP, :AMOUNT, :PRICE, :IMG)", array(
                    ":SKU" => $product->getSku(),
                    ":NAMEP" => $product->getName(),
                    ":DESCRIPTIONP" => $product->getDesc(),
                    ":AMOUNT" => $product->getAmount(),
                    ":PRICE" => $product->getPrice(),
                    ":IMG" => $product->getImage(),
                ));
            } catch (PDOException $e) {
                User::setMsg("NOTINSERTED_PRODUCT");
            }

            $return = $mysql->select("SELECT `id` FROM `product` WHERE `sku` = :SKU", array(
                ":SKU" => $product->getSku(),
            ));

            if (count($return[0]) > 0) {
                return $this->load($return[0]["id"]);
            } else {
                User::setMsg("NOTINSERTED_PRODUCT");
            }
        } else {
            User::setMsg("NOTDEFINED_PRODUCT");
        }
    }

    public function delete($product)
    {
        if (!empty($product) && isset($product)) {
            $rcp = new CategoryProductPDO();
            $rcp->deleteRelProduct($product->getId());

            $mysql = new Connection();
            try {
                $mysql->query("DELETE FROM `product` WHERE `id` = :ID", array(
                    ":ID" => $product->getId(),
                ));
            } catch (PDOException $e) {
                User::setMsg("NOTDELETED_PRODUCT");
            }
        } else {
            User::setMsg("NOTDEFINED_PRODUCT");
        }
    }

    public function update($product)
    {
        if (!empty($product) && isset($product)) {
            $mysql = new Connection();
            try {
                $mysql->query("UPDATE `product` SET `name` = :NAMEP, `description` = :DESCRIPTIONP, `amount` = :AMOUNT, `price` = :PRICE, `sku` = :SKU, `image` = :IMG WHERE `id` = :ID", array(
                    ":ID" => $product->getId(),
                    ":SKU" => $product->getSku(),
                    ":NAMEP" => $product->getName(),
                    ":DESCRIPTIONP" => $product->getDesc(),
                    ":AMOUNT" => $product->getAmount(),
                    ":PRICE" => $product->getPrice(),
                    ":IMG" => $product->getImage(),
                ));
            } catch (PDOException $e) {
                User::setMsg("NOTUPDATED_PRODUCT");
            }

            return $this->load($product->getId());

        } else {
            User::setMsg("NOTDEFINED_PRODUCT");
        }
    }

    public function validateProduct($product)
    {
        if (!empty($product) && isset($product)) {
            strlen($product->getSku()) > 9 ? User::setMsg("PRODUCTPDO_SKULENGTH") : "";
            strlen($product->getName()) > 130 ? User::setMsg("PRODUCTPDO_NAMELENGTH") : "";
            strlen($product->getAmount()) > 3 ? User::setMsg("PRODUCTPDO_QUANTITYLENGTH") : "";
            strlen($product->getDesc()) > 150 ? User::setMsg("PRODUCTPDO_DESCRIPTIONLENGTH") : "";
        } else {
            User::setMsg("NOTDEFINED_PRODUCT");
        }
    }
}
