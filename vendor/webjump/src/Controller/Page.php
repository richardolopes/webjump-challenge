<?php

namespace Webjump\Controller;

use \Rain\Tpl;
use \Webjump\Controller\User;

class Page
{
    private $tpl; // Para ter acesso em outros metodos.
    private $options = array();
    private $defaults = array( // Variaveis 'padrao'
        "msg" => true,
        "header" => true,
        "scripts" => true,
        "title" => "",
        "footer" => true,
        "data" => array(), // Variaveis do template.
    );

    public function __construct($opts = array(), $tpl_dir = "/views/")
    {
        // $opts -> Variaveis da rota.
        // $tpl_dir -> Pasta do template

        // Sobrescrever arrays
        $this->options = array_merge($this->defaults, $opts);

        // Configuração do RainTpl
        $config = array(
            "tpl_dir" => $_SERVER["DOCUMENT_ROOT"] . $tpl_dir,
            "cache_dir" => $_SERVER["DOCUMENT_ROOT"] . "/views-cache/",
            "debug" => false, // set to false to improve the speed
        );

        Tpl::configure($config);

        $this->tpl = new Tpl;

        // Atribuir as variaveis do template.
        $this->setDataTemplate($this->options["data"]);

        if ($this->options["header"] === true) {
            $this->tpl->assign("title", $this->options["title"]);
            $this->tpl->draw("header");
        }

        if ($this->options["msg"] === true) {
            $this->tpl->assign("msg", User::getMsg());
            $this->tpl->draw("msgs");
        }
    }

    private function setDataTemplate($data = array())
    {
        // Atribuir variaveis do template.
        foreach ($data as $key => $value) {
            $this->tpl->assign($key, $value);
        }
    }

    public function setTpl($name, $data = array(), $returnHTML = false)
    {
        // $name -> Nome do template.
        // $data -> Variaveis
        // $returnHTML -> Retornar o HTML ou apenas executar.
        $this->setDataTemplate($data);

        return $this->tpl->draw($name, $returnHTML);
    }

    public function __destruct()
    {
        if ($this->options["scripts"] === true) {
            $this->tpl->draw("scripts");
        }

        if ($this->options["footer"] === true) {
            $this->tpl->draw("footer");
        }
    }
}
