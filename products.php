<?php

session_start();

$_SESSION["page"] = "products.php";

require_once "vendor/autoload.php";
require_once "functions.php";

use \Webjump\Controller\Page;
use \Webjump\View\ProductView;

$products = ProductView::listProducts();

$page = new Page(["title" => "Products"]);
$page->setTpl("products", array(
    "products" => $products,
));
